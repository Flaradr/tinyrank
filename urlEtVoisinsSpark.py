import json

infile = open('Main.json', 'r',encoding="utf-8") #Name of the input file
outfileNeighbor = open('url_Spark.txt','w',encoding="utf-8") #Name of the output with neighbor 
outfilePR = open('PR_Spark.txt','w',encoding="utf-8") #Name of the output with PR 

data = json.load(infile)
for line in data:

	#Line that will be written in the file outfileNeighbor
	currentUrl = ""
	tempLine = "" 
	
	#Find the url of the website that you want to analyse
	try:
		urlAnalysed = line['Envelope']['WARC-Header-Metadata']['WARC-Target-URI']
		print("Value of the URI : " + urlAnalysed)
		outfilePR.write(urlAnalysed + '\t1\n')
		currentUrl = urlAnalysed
	except KeyError:
		print("No URI found for the envelope")
		
	#Find the neighbor of the website
	try:
		linkUrls =line['Envelope']['Payload-Metadata']['HTTP-Response-Metadata']['HTML-Metadata']['Links']
		for url in linkUrls:
			if currentUrl != "" and len(url['url']) > 2:
				outfileNeighbor.write(url['url'] + '\t' + currentUrl +'\n')
	except KeyError:
		print("No url found in the file for the current url")
