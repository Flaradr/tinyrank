# TinyRank

## Utilisation de PIG

Pour utiliser le pagerank de Pig, il faut :
1) Aller dans le fichier `implémentation_Pig`.
2) Ouvrir son terminal à l'intérieur du dossier.
3) Exécuter la commande `./script.sh [nombre d'itéation]`, par exemple pour 10 itération : `./script.sh 10`.

`/!\ Pig doit être installé sur votre machine, pour plus d'information voir :` https://www.edureka.co/blog/apache-pig-installation `/!\`

4) Après l'éxécution du script, déplacer le fichier `fichierfinal` dans le dossier `implémentation_Spark`.

## Utilisation de Spark

1) Aller dans le fichier `implémentation_Spark`.
2) Exécuter la commande `jupyter notebook`.

`/!\ Spark doit être installé et configuré à Jupyter Notebook sur votre machine, pour plus d'information voir :`  
https://medium.com/@singhpraveen2010/install-apache-spark-and-configure-with-jupyter-notebook-in-10-minutes-ae120ebca597 `/!\`

3) Le résultat s'affiche alors dans Jupyter Notebook lors de l'exécution du code.

## Informations supplémentaire sur les données.
Certains fichiers sont trop volumineux (URL et voisins pour Spark et Pig)   
On peut les trouver ici : https://uncloud.univ-nantes.fr/index.php/s/zBkonT7a8bwnnP3