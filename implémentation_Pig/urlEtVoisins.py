import json

infile = open('BACKUP/example3.json', 'r') #Name of the input file
outfileNeighbor = open('BACKUP/url.txt','w') #Name of the output with neighbor 
outfilePR = open('BACKUP/PR.txt','w') #Name of the output with PR 

data = json.load(infile)
for line in data:
	#Line that will be written in the file outfileNeighbor
	tempLine = "" 
	
	#Find the url of the website that you want to analyse
	try:
		urlAnalysed = line['Envelope']['WARC-Header-Metadata']['WARC-Target-URI']
		print("Value of the URI : " + urlAnalysed)
		outfilePR.write(urlAnalysed + ',1\n')
		tempLine = urlAnalysed 
	except KeyError:
		print("No URI found for the envelope")
		
	#Find the neighbor of the website
	try:
		linkUrls =line['Envelope']['Payload-Metadata']['HTTP-Response-Metadata']['HTML-Metadata']['Links']
		for url in linkUrls:
			tempLine = tempLine + ',' + url['url']
			#print(url['url'])
	except KeyError:
		print("No url found in the file for the current url")
	
	#Ecriture dans le fichier des voisins -> URI ,v1,v2,...,vn
	outfileNeighbor.write(tempLine +'\n')
