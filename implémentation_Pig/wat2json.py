import re

pattern = re.compile("^{") #Each line that match this pattern start with {

#Ajouter encoding="utf-8" une fois sur deux apres 'r' et 'w'...Python de merde
infile = open('foo.warc.wat','r') #Name of the input file
outfile = open('BACKUP/example3.json','w') #Name of the output file

outfile.write('[\n')
with infile, outfile:
	for line in infile:
		if pattern.match(line):
			outfile.write(line+',')
	outfile.write('\n]')
#TODO : Regler probleme virgule en fin de fichier...Changer de facon d'iterer?
