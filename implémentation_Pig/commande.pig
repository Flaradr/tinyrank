--UTILISATION DE PIG

-- Chargement des fichiers.
pg = load 'PR_Pig.txt' USING PigStorage(',') as (url: chararray, rank: double);
pg_distinct = Distinct pg;

-- Jointure des deux fichiers.
urls = load 'url_Pig.txt' USING PigStorage('\t') as (url: chararray, links:{link:(url:chararray)});
urls_distinct = DISTINCT urls; 

-- jointure des deux fichiers.
res = join pg_distinct by url, urls_distinct by url;

-- calcul du page rank pour chaque arc :

outbound_pagerank = foreach res generate (double)rank/COUNT(links) as rank, flatten(links) as to_url;
outbound_pagerank_distinct = DISTINCT outbound_pagerank;
outbound_pagerank_no = filter outbound_pagerank by (rank is not null);

-- cogroup : 
cogrpd = cogroup outbound_pagerank_no by to_url, res by pg_distinct::url INNER;

--nouveau rang
new_rank = foreach cogrpd generate group as url, (0.15 + 0.85 * SUM(outbound_pagerank_no.rank)) as ranks, flatten(res.urls_distinct::links) as links;

--DESCRIBE new_rank;
res = FOREACH new_rank GENERATE url, ranks;
DUMP res;
--Stockage du resultat : 
--STORE new_rank INTO 'resOut' USING PigStorage('\t');













