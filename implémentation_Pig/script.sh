#!/bin/bash
# Script calculant le page rank et le temps entre chaque itérations.

#Itération (choisis par l'utilisateur en argument de la commande).
let i=0

while [[ $i -lt $1 ]]; do
    #Lancement de la commande pour exécuter Pig et redirection dans le fichier 'output.txt'.
    /usr/bin/time --format='%e' -o performance/tmp"$i".txt ./pig -x local commande.pig > output.txt
    #Préparation du fichier de sortie > fichier d'entrée.
    #Suppression de la première ligne du fichier.
    #On supprime la ligne "Run with Java JRE"
    sed '1d' output.txt
    #Suppression du caractère '(' au début de chaque ligne.
    sed -i 's/(//g' output.txt
    #Suppression du caractère ')' à la fin de chaque ligne.
    sed -i 's/)//g' output.txt
    #Renommage du fichier pour la nouvelle itération.
    mv output.txt PR_Pig.txt
    #On stocke le temps dans un fichier pour l'intégration et la comparaison avec python.
    let i=i+1
done
#On supprime la ligne "Run with Java JRE"
sed '1d' PR_Pig.txt
#On créer le fichier contenant chaque temps entre chaque itérations.
cat /performance/* >> fichierfinal


